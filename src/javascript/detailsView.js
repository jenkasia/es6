import View from './view';

class DetailsView extends View {
  details;
  onsubmit;

  constructor(details, handler) {
    super();

    this.details = details;
    this.onsubmit = handler;
  }

  createDetailsWindow() {
    let root = document.getElementById('root');

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter-details'
    });

    let detailsForm = this.createForm();

    this.element.append(detailsForm);
    root.append(this.element);
  }

  createForm() {
    let root = document.getElementById('root');
    let { attack, defense, health, name, _id: id } = this.details;
    let stats = {attack, defense, health};
    let detailsForm = this.createElement({
      tagName: 'form',
      className: 'fighter-details-form'
    });
    detailsForm.addEventListener('submit', event => this.onsubmit(event, id));

    let nameLabel = this.createElement({
      tagName: 'div',
      className: 'fighter-name'
    });
    nameLabel.innerText = `${name}'s stats:`;
    
    detailsForm.append(nameLabel);

    let submitButton = this.createElement({
      tagName: 'input',
      className: 'form-button',
      attributes: {
        type: 'submit',
        value: 'Save changes',
        id: 'submit-btn'
      }
    });

    Object.keys(stats).forEach(attrName => {
      let formRow = this.createElement({
        tagName: 'div',
        className: 'form-row'
      });

      let attrLabel = this.createAttributeLabel(attrName);
      let attrInput = this.createAttributeInput(attrName, stats[attrName], submitButton);

      formRow.append(attrLabel, attrInput);
      detailsForm.append(formRow);
    });

    let buttonRow = this.createElement({
      tagName: 'div',
      className: 'form-row'
    });

    let closeButton = this.createElement({
      tagName: 'button',
      className: 'form-button'
    });
    closeButton.innerText = 'Close';
    closeButton.addEventListener('click', () => {
      root.removeChild(this.element);
    });

    buttonRow.append(closeButton, submitButton);
    detailsForm.append(buttonRow);
    return detailsForm;
  }

  createAttributeLabel(attributeName) {
    let label = this.createElement({
      tagName: 'label',
      className: 'attr-label',
      attributes: {
        ['for']: `${attributeName}-data`
      }
    });
    label.innerText = `${attributeName[0].toUpperCase() + attributeName.slice(1)}:`;
    return label;
  }

  createAttributeInput(attributeName, attributeValue, buttonElement) {
    let input = this.createElement({
      tagName: 'input',
      className: 'attr-input',
      attributes: {
        type: 'number',
        name: attributeName,
        value: attributeValue,
        id: `${attributeName}-data`
      }
    });
    input.addEventListener('change', () => {
      buttonElement.style.visibility = 'visible';
    });
    return input;
  }
}

export default DetailsView;