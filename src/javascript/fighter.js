class Fighter {

   constructor(info) {
      this.name = info.name
      this.health = info.health
      this.attack = info.attack
      this.defense = info.defense
   }

   getHitPower() {
      let criticalHitChance = Math.random() + 1
      return criticalHitChance * this.attack
   }

   getBlockPower() {
      let dodgeChance = Math.random() + 1
      return dodgeChance * this.defense
   }
}

export default Fighter;