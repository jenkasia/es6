import View from './view';
import FighterView from './fighterView';
import DetailsView from './detailsView';
import { fighterService } from './services/fightersService';
import Fighter from "./fighter";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleSelection);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async obtainDetails(fighterId) {
    let info;
    if (!this.fightersDetailsMap.has(fighterId)) {
    info = await fighterService.getFighterDetails(fighterId);
    this.fightersDetailsMap.set(fighterId, info);
    } else {
      info = this.fightersDetailsMap.get(fighterId);
    }
    return info;
  }

  async handleFighterClick(event, fighter) {
    let info = await this.obtainDetails(fighter._id);
    const detailsView = new DetailsView(info, this.handleSubmission);
    detailsView.createDetailsWindow(info);
  }

}

export default FightersView;
